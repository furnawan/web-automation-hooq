CODECEPTION
Reference: https://codeception.com/docs/modules/WebDriver


 1. Set ENV
	Location: /tests/acceptance/_bootstrap.php

2. Set Executor
	Location: /tests/acceptance/_bootstrap.php

3. Set Credential
	Location: /tests/acceptance/_bootstrap.php

4. Set Register Email
	Location: /tests/acceptance/_bootstrap.php
	For list of domain https://temp-mail.org/en/option/change

5. Set Email Report
	Location: /mail.php

6. Set Slack Report to #deploy
	Location: /codeception.yml

7. How to run:
	1. Install composer whenever you pulled from master by running:
		'php composer.phar install'
	2. Run driver:
		'phantomjs --webdriver=4444'
		'chromedriver --url-base=/wd/hub'
		'selenium-server -port 4444 -debug'

	3. Run automation with debug:
		'$ php codecept.phar run acceptance --debug'
	4. Run automation and create html report:
		'$ php codecept.phar run acceptance --report --html
	5. Send html report to email list:
		'$ php mail.php'

