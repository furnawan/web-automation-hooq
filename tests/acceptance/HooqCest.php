<?php
use Step\Acceptance\User\HooqRegister;
use \Codeception\Test\Unit;

class HooqCest
{
    public function register(HooqRegister $auth)
    {
        $auth->wantTo('register with valid email');
        $auth->registerWithValidEmail();

        $auth->wantTo('login to Gmail to verify my account');
        $auth->loginGmail();
    }

}
