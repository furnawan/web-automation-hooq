<?php
namespace Step\Acceptance\User;

class HooqRegister extends \AcceptanceTester
{
    public function registerWithValidEmail()
    {
        //REGISTER
        $I = $this;
        $I->amOnUrl(APP_URL);
        $I->amOnPage('/id/signup-email');
        // $I->saveScreenshot('Signup page.png');

        $I->see('DAFTAR');

        $I->seeElement('input', ['name' => 'email']);
        $I->see('Dengan mendaftar saya setuju kepada syarat dan ketentuan HOOQ.');
        $I->waitForElement('#submit-button', 30);

        $email = REGISTER_EMAIL;

        $I->wait(5);
        
        $I->fillField('email', $email);
        $I->wait(5);

        $I->click('#submit-button');

        $I->wait(5);
        // $I->saveScreenshot('Submit email success.png');
            
    }

    public function loginGmail()
    {
        //REGISTER
        $I = $this;
        $I->amOnUrl('https://accounts.google.com/AccountChooser?service=mail&continue=https://mail.google.com/mail/');

        // $I->click('/html/body/nav/div/a[2]');

        $I->wait(5);

        $I->seeElement('input', ['id' => 'identifierId']);

        $identifierId = LOGIN_EMAIL;
        $password = LOGIN_PASSWORD;
        $recoveryIdentifierId = LOGIN_EMAIL;

        $I->wait(5);
        
        $I->appendField('#identifierId', $identifierId);

        //$I->click('//*[@id="identifierNext"]/content/span');
        $I->executeJS('return document.getElementsByClassName("CwaK9")[0].click()');

        $I->wait(3);

        $I->seeElement('input', ['name' => 'password']);
        $I->appendField('password', $password);

        //$I->click('//*[@id="passwordNext"]/content/span');
        $I->executeJS('return document.getElementsByClassName("CwaK9")[0].click()');
        $I->wait(5);

        // $I->see('HOOQ - Your verification code');

        // yP for read email
        // zF for unread email
        $I->waitForElement('.yP', 5);
        $I->seeElement('.yP');
        $I->executeJS('return document.getElementsByClassName("yP")[0].click()');
        $I->wait(5);     

        $I->see('Confirm Email');
        $I->click('Confirm Email');

        $I->wait(5);
        $I->see('VERIFIKASI SELESAI!');
        $I->wait(5);


    }

}
